package ru.leonenkov.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created on 29.10.2014.
 */
public class Md5phpTest {
    @Test
    public void md5HashTest() {
        String actual   = Md5php.md5("All work and no play makes Jack a dull boy.");
        String expected = "d8f72233c67a68c5ec2bd51c6be7556e";
        Assert.assertEquals(actual, expected);
    }
}
